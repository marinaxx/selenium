import org.omg.PortableInterceptor.HOLDING;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.annotations.Test;

public class SeleniumTest {
    @Test
    public void googleIt()throws InterruptedException{
        System.setProperty("webdriver.chrome.driver","/Users/marina/SeleniumWebDriver/src/main/resources/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.google.com/");

        driver.manage().window().maximize();
        driver.getCurrentUrl();
        Thread.sleep(3000);

        HomePage namecheap = new HomePage(driver);
        namecheap.getSearch().sendKeys("namecheap");

        Thread.sleep(2000);
        namecheap.getHitSearch().click();

        Thread.sleep(2000);
        namecheap.getSelectWebPage().click();

        Thread.sleep(2000);
        namecheap.getCheckLogoIsDisplayed().isDisplayed();

        Thread.sleep(2000);
        WebElement Element = driver.findElement(HomePage.DOMAIN_BUTTON_by);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", Element);

        Thread.sleep(2000);
        namecheap.getDomains().equals("Domains");
        namecheap.getDomains().click();
        driver.close();
    }
    @Test
    public void matchTitle () throws InterruptedException{
        System.setProperty("webdriver.chrome.driver","/Users/marina/SeleniumWebDriver/src/main/resources/chromedriver");
        WebDriver driver = new ChromeDriver();
        HomePage home = new HomePage(driver);

        driver.get("https://kharkiv.ithillel.ua/");
        driver.manage().window().maximize();

        String expected = driver.getTitle();
        System.out.println(expected);

        if(expected.equals("Компьютерная школа Hillel в Харькове: курсы IT технологий")){
            System.out.println("The title is " + "Компьютерная школа Hillel в Харькове: курсы IT технологий");
        }
        else{
            System.out.println("The title does not match the specification");
        }
        Thread.sleep(2000);
        home.getSelectManualTesting().click();

        Thread.sleep(2000);
        System.out.println("URL is "+driver.getCurrentUrl());
        driver.close();
    }
    @Test
    public void surfAmazon() throws InterruptedException{
        System.setProperty("webdriver.chrome.driver","/Users/marina/SeleniumWebDriver/src/main/resources/chromedriver");
        WebDriver surf = new ChromeDriver();

        surf.get("https://www.amazon.com/");

        HomePage amazon = new HomePage(surf);
        amazon.getSelectShoppingCard().click();

        Thread.sleep(2000);
        surf.navigate().back();
        Thread.sleep(2000);
        surf.navigate().forward();
        Thread.sleep(2000);
        surf.navigate().refresh();
        Thread.sleep(2000);
        surf.close();
    }
    @Test
    public void checkBox()throws InterruptedException{
        System.setProperty("webdriver.chrome.driver","/Users/marina/SeleniumWebDriver/src/main/resources/chromedriver");
        WebDriver check = new ChromeDriver();
        check.get("https://healthfirst.org/brokers/");
        HomePage home = new HomePage(check);
        //Find element by link text and store in variable "Element"
        WebElement Element = check.findElement(By.linkText("Medicare Advantage Plans"));

        //This will scroll the page till the element is found
        JavascriptExecutor js = (JavascriptExecutor) check;
        js.executeScript("arguments[0].scrollIntoView();", Element);

        Thread.sleep(2000);
        home.getSelectMedicareAdvantagePlansTab().click();

        Thread.sleep(2000);
        home.getSelectJoinOurMailingList().click();
        WebElement RadioButton = check.findElement(By.id("Medicare_advantage_Plans"));
        if(RadioButton.isDisplayed()){
            System.out.println("Radio button is displayed");
        }
        else{
            System.out.println("Radio button is not displayed");
        }
    }
    @Test
    public void checkIfenabled()throws InterruptedException{
        System.setProperty("webdriver.chrome.driver","/Users/marina/SeleniumWebDriver/src/main/resources/chromedriver");
        WebDriver check = new ChromeDriver();
        check.get("https://healthfirst.org/brokers/");
        HomePage insurance = new HomePage(check);

        Thread.sleep(2000);
        insurance.getSelectMedicareAdvantagePlansTab().click();

        Thread.sleep(2000);
        insurance.getSelectJoinOurMailingList().click();

        WebElement RadioButton = check.findElement(HomePage.SMALL_BUSINESS_by);
        if(RadioButton.isEnabled()){
            System.out.println("Radio button is enabled");
            RadioButton.click();
        }
        else{
            System.out.println("Radio button is not enabled");
        }
        if(RadioButton.isSelected()){
            System.out.println("Is selected");
        }
        else{
            System.out.println("did not work");
        }
    }

}
