import com.sun.xml.internal.rngom.parse.host.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    public HomePage (WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    public final static String INPUT = "//*[@id='tsf']/div[2]/div[1]/div[1]/div/div[2]/input";
    public final static String HITSEARCH = "//*[@id='tsf']/div[2]/div[1]/div[2]/div[2]/div[2]/center/input[1]";
    public final static String SELECT = "//*[@id='rso']/div[1]/div/div/div/div/div[1]/a[1]/div/cite";
    public final static String LOGO = "//html/body/fragment[1]/layout-header/div/header/div/div[2]/div/div/div/div[1]/strong/a/img";
    public final static String DOMAINS = "/html/body/fragment[3]/layout-footer/div/footer/div[1]/div/div[1]/div[2]/div/div[1]/div[1]/b/a/span";
    public final static By DOMAIN_BUTTON_by = By.xpath("/html/body/fragment[3]/layout-footer/div/footer/div[1]/div/div[1]/div[2]/div/div[1]/div[1]/b/a/span");
    public final static String SELECT_TESTING = "/html/body/section[3]/div/ul/li[2]/a";
    public final static String SELECT_SHOPPING_CARD = "//*[@id='nav-cart']/span[2]";
    public final static String SELECT_MEDICARE_ADVANTAGE_PLANS_TAB = "//*[@id='work_with_us']/div/p/a[2]";
    public final static String SELECT_JOIN_OUR_MAILING_LIST = "//*[@id='medicare_plans1']/div/div/a";
    public final static By SMALL_BUSINESS_by = By.xpath("//*[@id='Small_Business_Plans']");

    @FindBy(xpath = SELECT_MEDICARE_ADVANTAGE_PLANS_TAB)
    private WebElement selectMedicareAdvantagePlansTab;

    @FindBy(xpath = SELECT_JOIN_OUR_MAILING_LIST)
    private WebElement selectJoinOurMailingList;
    @FindBy(xpath = SELECT_SHOPPING_CARD)
    private WebElement selectShoppingCard;

    @FindBy(xpath = SELECT_TESTING)
    private WebElement selectManualTesting;

    @FindBy(xpath = DOMAINS)
    private WebElement domains;

    @FindBy(xpath = LOGO)
    private WebElement checkLogoIsDisplayed;

    @FindBy(xpath = SELECT)
    private WebElement selectWebPage;

    @FindBy(xpath = HITSEARCH)
    private WebElement hitSearch;

    @FindBy(xpath = INPUT)
    private WebElement input;

    public WebElement getSearch() {
        return input;
    }
    public WebElement getHitSearch() {
        return hitSearch;
    }
    public WebElement getSelectWebPage(){
            return selectWebPage;
        }

    public WebElement getCheckLogoIsDisplayed() {
        return checkLogoIsDisplayed;
    }

    public WebElement getDomains() {
        return domains;
    }

    public WebElement getSelectManualTesting() {
        return selectManualTesting;
    }

    public WebElement getSelectShoppingCard() {
        return selectShoppingCard;
    }

    public static String getINPUT() {
        return INPUT;
    }

    public WebElement getSelectMedicareAdvantagePlansTab() {
        return selectMedicareAdvantagePlansTab;
    }

    public WebElement getSelectJoinOurMailingList() {
        return selectJoinOurMailingList;
    }
}

